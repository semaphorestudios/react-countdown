import React, {Component} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Timer from "./components/Timer";
import InputTimer from "./components/TimerInput";
import StartButton from "./components/StartButton";
import StepButton from "./components/StepButton";

export default class App extends Component{

    constructor(props) {
        super(props);
        this.state = {
            seconds: '00',
            value: '00',
            incrementValue: 0,
            isClicked: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.startCountDown = this.startCountDown.bind(this);
        this.incrementCountDown = this.incrementCountDown.bind(this);
        this.tick = this.tick.bind(this);
    }

    handleChange(event) {
        this.setState({
            // On user interaction with the input fields, pull the newly entered value from the 'event.target' variable
            // and assign to the 'this.state.value' variable using the this.setState function.
            value: event.target.value,
        })
    }

    tick() {
        // This function is run every 1000 milliseconds as set on line 60
        var minute = Math.floor(this.secondsRemaining / 60);
        var second = this.secondsRemaining - (minute * 60);

        // Reset the state properties with the new values as the timer counts down
        this.setState({
            value: minute,
            seconds: second,
        });

        // If number of seconds is below 10, we modify the value and add a leading '0' to maintain the 2 digit format
        if (second < 10) {
            this.setState({
                seconds: "0" + this.state.seconds,
            })
        }

        // If number of minutes is below 10, we modify the value and add a leading '0' to maintain the 2 digit format
        if (minute < 10) {
            this.setState({
                value: "0" + minute,
            })
        }

        // If the number of minutes and seconds are both at 0 we clear the interval, this stops the count down timer
        // from continuing to count into negative
        if (minute === 0 && second === 0) {
            clearInterval(this.intervalhandle);
            this.setState({
                isClicked: false
            })
        }

        // At the end of each cycle we modify the 'secondsRemaining' variable to subtract 1 from itself, i.e countdown
        this.secondsRemaining--
    }

    startCountDown() {
        // Set the interval to run every 1000 milliseconds (1 second)
        this.intervalhandle = setInterval(this.tick, 1000);

        // create variable 'time' that holds value from input field
        // set 'secondsRemaining' variable to seconds value by multiplying by 60
        // mark 'isClicked' as true
        let time = this.state.value;
        this.secondsRemaining = time * 60;
        this.setState({
            isClicked: true
        })
    }

    incrementCountDown(event) {

        // Get the incremental value of the corresponding step button that was clicked
        let timeIncrement = event.target.value;

        // Check if there is already a value assigned to the 'this.secondsRemaining' variable. This means the user has
        // either entered a value in the field manually and started the countdown or they have clicked one of the step
        // buttons
        if (this.secondsRemaining) {

            // If there is already a value assigned to 'this.secondsRemaining' we modify the app variable by
            // converting the step button to seconds (timeIncrement * 60)
            // then adding the previous number of seconds and re-assigning to the 'this.secondsRemaining' variable
            this.secondsRemaining = (timeIncrement * 60) + this.secondsRemaining;
            this.setState({
                // Having calculated the new value of seconds remaining, we then reset the 'this.value' variable to the
                // new total number of minutes by taking this newly calculate value from 'this.secondsRemaining' and
                // dividing by 60
                value: this.secondsRemaining / 60
            });

        } else {

            // In the case where user has not started the count down after interacting with the input field or clicked
            // one of the step buttons, we are simply adding the initial values to 'this.secondsRemaining' and
            // 'this.value'
            this.secondsRemaining = timeIncrement * 60;
            this.setState({
                value: timeIncrement
            });
        }
    }

    render() {
        const clicked = this.state.isClicked;
            return (
                <div>
                    <div className={"row"}>
                        <div className={"col-md-4"}></div>
                        <div className={"col-md-4"}>
                            {/*Initialize a Input Timer (Input Field)
                            value: pass thought the state property that you want the value assigned to
                            handleChange: pass through the function that should be executed when a change event occurs
                                          in the input field
                            */}
                            <div className={"input-group mb-3"}>
                            <InputTimer value={this.state.value} handleChange={this.handleChange}/>

                            {/*Initialize a Start Button
                            startCountDown: pass through the function that you want to be assigned to the the
                                            'startCountDown' state property. Without this, the start button component
                                            will not be able to identify the function
                            value: pass through the state property 'value' so that the button is able to determine if
                                   the value null or not to switch the disabled feature
                            */}
                            <StartButton startCountDown={this.startCountDown} isClicked={this.isClicked} value={this.state.value}/>
                            </div>
                            <div className={"btn-group"} role={"group"} aria-label={"Incremental Step Buttons"}>
                                <StepButton incrementValue={2} incrementCountDown={this.incrementCountDown} />
                                <StepButton incrementValue={5} incrementCountDown={this.incrementCountDown} />
                                <StepButton incrementValue={10} incrementCountDown={this.incrementCountDown} />
                            </div>
                            {/*Initialize a Timer Component
                            value: this is the value you enter in the input field
                            seconds: this is the value after it has been converted into seconds

                            By pulling the values from 'this.state.value or this.state.seconds' we are able to
                            dynamically update the display without having to re-render the timer component
                            */}
                            <Timer value={this.state.value} seconds={this.state.seconds}/>


                        </div>
                    </div>

                </div>
            );
    }
}
