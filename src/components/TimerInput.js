import React, {Component} from "react"

export default class InputTimer extends Component {
    render() {
        return (
            <React.Fragment>
                <div>
                    {/*Input field
                    value: assigns the value entered to the property of 'value'
                    onChange: marks the field with a function that should be run when the user interacts with the field
                    required: user has to enter a value in the field
                    */}
                    <input className={"form-control"} type="number" value={this.props.value} onChange={this.props.handleChange} required/>
                </div>
            </React.Fragment>
        )
    }

}