import React, {Component} from "react";

export default class StartButton extends Component{

    render() {
        return (
            <React.Fragment>
                <div className= {"input-group-append"}
                >
                    {/*Start Button
                    disabled: mark the button as 'disabled' if the app property 'value' is not valid (i.e empty)
                    onClick: mark the button with a function that should be executed when the user clicks
                    */}
                    <button className="btn btn-outline-secondary" disabled={this.props.isClicked} onClick={this.props.startCountDown}>Start</button>
                </div>
            </React.Fragment>
        )
    }

}