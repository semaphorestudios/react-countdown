import React, {Component} from "react";

export default class Timer extends Component{
    render() {
        return (
            <React.Fragment>
                <div>
                    <h1>{this.props.value}:{this.props.seconds}</h1>
                </div>
            </React.Fragment>
        )
    }

}