import React, {Component} from "react";

export default class StepButton extends Component{

    render() {
        return (
            <React.Fragment>
                <button type="button" className={"btn btn-secondary"} value={this.props.incrementValue} onClick={this.props.incrementCountDown}>+ {this.props.incrementValue} Minutes</button>
            </React.Fragment>
        )
    }

}